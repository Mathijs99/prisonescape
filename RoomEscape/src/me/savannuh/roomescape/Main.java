package me.savannuh.roomescape;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;
import org.bukkit.Location;

@SuppressWarnings("deprecation")
public class Main extends JavaPlugin implements Listener {

	ArrayList<String> criminalList = new ArrayList<String>();
	ArrayList<String> policeList = new ArrayList<String>();

	ItemStack magicKey = new ItemStack(Material.SEA_PICKLE);
	ItemMeta magicKeyMeta = magicKey.getItemMeta();

	ItemStack magicSword = new ItemStack(Material.DIAMOND_SWORD);
	ItemMeta magicSwordMeta = magicSword.getItemMeta();

	ArrayList<Player> caughtList = new ArrayList<Player>();

	private Scoreboard scoreboard;
	private Team team;

	public void onEnable() {

		System.out.println(ChatColor.GREEN + "De Room Escape plugin is geactiveerd.");
		Bukkit.getServer().getPluginManager().registerEvents(this, this);

		scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();

		team = scoreboard.getTeam("HideNameTags");

		if (team == null) {

			scoreboard.registerNewTeam("HideNameTags");
			team = scoreboard.getTeam("HideNameTags");
		}

	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {

			sender.sendMessage(ChatColor.RED + "Alleen spelers mogen dit commando gebruiken.");
			return true;

		}

		Player player = (Player) sender;

		if (!player.isOp()) {

			player.sendMessage(ChatColor.RED + "U heeft geen toestemming voor dit commando.");
			return true;

		}

		if (cmd.getName().equalsIgnoreCase("setspawn")) {

			if (args[0].equalsIgnoreCase("politie") && args.length == 1) {

				player.sendMessage(ChatColor.GREEN + "De spawn van de polities is succesvol opgeslagen.");
				getConfig().set("spawnPolitie.world", player.getLocation().getWorld().getName());
				getConfig().set("spawnPolitie.x", player.getLocation().getX());
				getConfig().set("spawnPolitie.y", player.getLocation().getY());
				getConfig().set("spawnPolitie.z", player.getLocation().getZ());
				saveConfig();
				return true;

			}
			if (args[0].equalsIgnoreCase("dief") && args.length == 1) {

				player.sendMessage(ChatColor.GREEN + "De spawn van de dieven is succesvol opgeslagen.");
				getConfig().set("spawnDief.world", player.getLocation().getWorld().getName());
				getConfig().set("spawnDief.x", player.getLocation().getX());
				getConfig().set("spawnDief.y", player.getLocation().getY());
				getConfig().set("spawnDief.z", player.getLocation().getZ());
				saveConfig();
				return true;

			}
			if (args[0].equalsIgnoreCase("home") && args.length == 1) {

				player.sendMessage(ChatColor.GREEN + "De home-spawn is succesvol opgeslagen.");
				getConfig().set("spawnHome.world", player.getLocation().getWorld().getName());
				getConfig().set("spawnHome.x", player.getLocation().getX());
				getConfig().set("spawnHome.y", player.getLocation().getY());
				getConfig().set("spawnHome.z", player.getLocation().getZ());
				saveConfig();
				return true;

			}

		}

		if (cmd.getName().equalsIgnoreCase("spawn")) {
			if (args[0].equalsIgnoreCase("politie") && args.length == 1) {
				if (getConfig().getConfigurationSection("spawnPolitie") == null) {

					player.sendMessage(
							ChatColor.RED + "Er is nog geen spawn van de polities. Gebruik /setspawn politie");
					return true;

				}
				player.sendMessage(ChatColor.GREEN + "U wordt geteleporteerd naar de spawn van de polities.");
				World world = Bukkit.getServer().getWorld(getConfig().getString("spawnPolitie.world"));
				double x = getConfig().getDouble("spawnPolitie.x");
				double y = getConfig().getDouble("spawnPolitie.y");
				double z = getConfig().getDouble("spawnPolitie.z");
				player.teleport(new Location(world, x, y, z));
				return true;

			}
			if (args[0].equalsIgnoreCase("dief") && args.length == 1) {
				if (getConfig().getConfigurationSection("spawnDief") == null) {

					player.sendMessage(ChatColor.RED + "Er is nog geen spawn van de dieven. Gebruik /setspawn dief");
					return true;

				}

				player.sendMessage(ChatColor.GREEN + "U wordt geteleporteerd naar de spawn van de dieven.");
				World world = Bukkit.getServer().getWorld(getConfig().getString("spawnDief.world"));
				double x = getConfig().getDouble("spawnDief.x");
				double y = getConfig().getDouble("spawnDief.y");
				double z = getConfig().getDouble("spawnDief.z");
				player.teleport(new Location(world, x, y, z));
				return true;

			}
		}
		if (args[0].equalsIgnoreCase("home") && args.length == 1) {
			if (getConfig().getConfigurationSection("spawnHome") == null) {

				player.sendMessage(ChatColor.RED + "Er is nog geen home-spawn. Gebruik /setspawn home");
				return true;

			}

			player.sendMessage(ChatColor.GREEN + "U wordt geteleporteerd naar de home-spawn.");
			World world = Bukkit.getServer().getWorld(getConfig().getString("spawnHome.world"));
			double x = getConfig().getDouble("spawnHome.x");
			double y = getConfig().getDouble("spawnHome.y");
			double z = getConfig().getDouble("spawnHome.z");
			player.teleport(new Location(world, x, y, z));
			return true;

		}
		return false;

	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {

		Material blockMaterial = event.getBlock().getType();

		if (blockMaterial.equals(Material.GLASS_PANE) || blockMaterial.equals(Material.WHITE_STAINED_GLASS_PANE)
				|| blockMaterial.equals(Material.COBBLESTONE)) {

			event.setDropItems(false);

			Bukkit.getScheduler().runTaskLater(this,
					() -> event.getBlock().getLocation().getBlock().setType(blockMaterial), 2000);

		} else {
			if (event.getPlayer().isOp()) {
				return;
			} else {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onFoodChange(FoodLevelChangeEvent event) {
		if (event.getFoodLevel() < 20) {

			event.setCancelled(true);

		}
	}

	@EventHandler
	public void onCreeperExplosion(EntityExplodeEvent event) {

		event.setCancelled(true);

	}

	@EventHandler
	public void onItemFrameRightClick(PlayerInteractEntityEvent event) {

		Entity entity = (Entity) event.getRightClicked();

		if (entity instanceof ItemFrame) {

			if (((ItemFrame) entity).getItem().getType() != Material.AIR) {

				event.getPlayer().getInventory().addItem(((ItemFrame) entity).getItem());
				event.setCancelled(true);

			}
		}
	}

	@EventHandler
	public void onPlayerFish(PlayerFishEvent event) {

		if (event.getCaught() instanceof Player && policeList.contains(event.getPlayer().getName())) {

			event.getCaught().teleport(event.getPlayer().getLocation());

			caughtList.remove(event.getCaught());

		}
	}

	@EventHandler
	public void onItemFrameBreak(EntityDamageByEntityEvent event) {

		if (event.getEntity() instanceof ItemFrame) {

			event.setCancelled(true);

		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {

		event.getPlayer().setDisplayName(ChatColor.WHITE + event.getPlayer().getName());
		event.getPlayer().setPlayerListName(ChatColor.WHITE + event.getPlayer().getName());

		if (getConfig().getConfigurationSection("spawnHome") != null) {

			World world = Bukkit.getServer().getWorld(getConfig().getString("spawnHome.world"));
			double x = getConfig().getDouble("spawnHome.x");
			double y = getConfig().getDouble("spawnHome.y");
			double z = getConfig().getDouble("spawnHome.z");

			event.setRespawnLocation(new Location(world, x, y, z));
		}

		policeList.remove(event.getPlayer().getName());
		criminalList.remove(event.getPlayer().getName());

	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		if ((event.getDamager() instanceof Player) && (event.getEntity() instanceof Player)) {

			Player damager = (Player) event.getDamager();
			Player target = (Player) event.getEntity();

			if (policeList.contains(damager.getName()) && policeList.contains(target.getName())) {

				damager.sendMessage(ChatColor.RED + "U kunt geen andere polities slaan.");
				event.setCancelled(true);

			} else if (!(policeList.contains(target.getName()) || criminalList.contains(target.getName()))) {

				damager.sendMessage(ChatColor.RED + "U kunt geen spelers slaan die geen politie of geen dief zijn.");
				event.setCancelled(true);

			} else if (caughtList.contains(damager)) {

				damager.sendMessage(ChatColor.RED + "U bent gevangen dus u kunt niemand meer slaan.");
				event.setCancelled(true);

			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {

		if (event.getPlayer().isOp()) {
			if (event.getPlayer().getInventory().getItemInMainHand().getItemMeta().equals(magicKeyMeta)) {
				event.setCancelled(true);
			} else {
				event.setCancelled(false);
			}
		} else {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {

		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getClickedBlock() == null) {
			return;
		}

		ItemStack hand = event.getPlayer().getInventory().getItemInMainHand();

		if (hand == null || hand.getItemMeta() == null || !hand.getItemMeta().equals(magicKeyMeta)) {

			if (event.getClickedBlock().getType().equals(Material.STONE_BUTTON)) {

				event.setCancelled(true);
				event.getPlayer()
						.sendMessage(ChatColor.RED + "U heeft een sleutel nodig om op deze knop te kunnen drukken.");
				return;

			} else if (event.getClickedBlock().getType().equals(Material.LEVER)) {

				event.setCancelled(true);
				event.getPlayer().sendMessage(
						ChatColor.RED + "U heeft een sleutel nodig om aan deze hendel te kunnen\ntrekken.");
				return;

			}
		}
	}

	@EventHandler
	public void onInteractEntity(PlayerInteractEntityEvent event) {

		if (event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.FISHING_ROD)) {

			if (event.getRightClicked() instanceof Player) {

				caughtList.add((Player) event.getRightClicked());

			}

		}

	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {

		if (this.getConfig().getString("ranks." + event.getPlayer().getUniqueId().toString()) == null) {

			this.getConfig().set("ranks." + event.getPlayer().getUniqueId().toString(), "�3�lSpeler");
			this.saveConfig();

		}

		if (getConfig().getConfigurationSection("spawnHome") != null) {

			World world = Bukkit.getServer().getWorld(getConfig().getString("spawnHome.world"));
			double x = getConfig().getDouble("spawnHome.x");
			double y = getConfig().getDouble("spawnHome.y");
			double z = getConfig().getDouble("spawnHome.z");
			event.getPlayer().teleport(new Location(world, x, y, z));
		}

		event.getPlayer().setDisplayName(ChatColor.WHITE + event.getPlayer().getName());
		event.getPlayer().setPlayerListName(ChatColor.WHITE + event.getPlayer().getName());
		event.getPlayer().setHealth(20);
		event.getPlayer().setGameMode(GameMode.SURVIVAL);
		event.getPlayer().setFoodLevel(20);

		magicKeyMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Magische " + ChatColor.GOLD + ""
				+ ChatColor.BOLD + "Sleutel");
		magicKey.setItemMeta(magicKeyMeta);
		event.getPlayer().getInventory().addItem(magicKey);
		event.getPlayer().getInventory().clear();

		policeList.remove(event.getPlayer().getName());
		criminalList.remove(event.getPlayer().getName());

		scoreboard.getTeam("HideNameTags").addPlayer(event.getPlayer());

		team.setNameTagVisibility(NameTagVisibility.NEVER);
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {

		if (event.getDrops().contains(magicSword) && event.getDrops().contains(magicKey)) {

			event.getDrops().clear();

			Random random = new Random();
			int number = random.nextInt(2);

			if (number == 0) {
				event.getDrops().add(magicKey);
			} else if (number == 1) {
				event.getDrops().add(magicSword);
			}
		} else {
			event.getDrops().clear();
		}
	}

	public void push(PlayerMoveEvent event) {

		Location to = event.getTo();
		Location from = event.getFrom();

		Vector direction = new Vector(from.getX() - to.getX(), from.getY() - to.getY(), from.getZ() - to.getZ())
				.multiply(10);
		event.getPlayer().setVelocity(direction);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {

		if (event.getTo().getBlock().getType().equals(Material.STONE_PRESSURE_PLATE)) {

			Player p = event.getPlayer();
			Location loc = p.getLocation();

			ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
			ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
			ItemStack leggins = new ItemStack(Material.LEATHER_LEGGINGS);
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);

			LeatherArmorMeta helmetMeta = (LeatherArmorMeta) helmet.getItemMeta();
			LeatherArmorMeta chestplateMeta = (LeatherArmorMeta) chestplate.getItemMeta();
			LeatherArmorMeta legginsMeta = (LeatherArmorMeta) leggins.getItemMeta();
			LeatherArmorMeta bootsMeta = (LeatherArmorMeta) boots.getItemMeta();

			if (loc.clone().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)
					&& (loc.clone().add(0, -2, 0).getBlock().getType().equals(Material.REDSTONE_BLOCK))) {

				if (getConfig().getConfigurationSection("spawnDief") == null) {
					p.sendMessage(ChatColor.RED + "U moet een spawn hebben. Gebruik /setspawn dief");
					push(event);
				} else {

					p.getInventory().clear();

					p.setDisplayName(ChatColor.RED + p.getName() + ChatColor.WHITE);
					p.setPlayerListName(ChatColor.RED + p.getName() + ChatColor.WHITE);
					p.setCustomNameVisible(true);

					ItemStack stick = new ItemStack(Material.STICK);
					ItemMeta stickMeta = (ItemMeta) stick.getItemMeta();
					stickMeta.addEnchant(Enchantment.KNOCKBACK, 3, true);
					;
					stickMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Stok");
					stick.setItemMeta(stickMeta);
					p.getInventory().addItem(stick);

					ItemStack pickaxe = new ItemStack(Material.WOODEN_PICKAXE);
					p.getInventory().addItem(pickaxe);

					helmetMeta.setColor(Color.RED);
					chestplateMeta.setColor(Color.RED);
					legginsMeta.setColor(Color.RED);
					bootsMeta.setColor(Color.RED);

					helmet.setItemMeta(helmetMeta);
					chestplate.setItemMeta(chestplateMeta);
					leggins.setItemMeta(legginsMeta);
					boots.setItemMeta(bootsMeta);

					p.getInventory().setHelmet(helmet);
					p.getInventory().setChestplate(chestplate);
					p.getInventory().setLeggings(leggins);
					p.getInventory().setBoots(boots);

					World world = Bukkit.getServer().getWorld(getConfig().getString("spawnDief.world"));
					double x = getConfig().getDouble("spawnDief.x");
					double y = getConfig().getDouble("spawnDief.y");
					double z = getConfig().getDouble("spawnDief.z");
					p.teleport(new Location(world, x, y, z));
					p.sendMessage(ChatColor.GREEN + "U wordt geteleporteerd naar de spawn van de dieven.");

					policeList.remove(p.getName());
					criminalList.add(p.getName());
				}
			} else if (loc.clone().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)
					&& (loc.clone().add(0, -2, 0).getBlock().getType().equals(Material.LAPIS_BLOCK))) {

				if (getConfig().getConfigurationSection("spawnPolitie") == null) {
					p.sendMessage(ChatColor.RED + "U moet een spawn hebben. Gebruik /setspawn politie");
					push(event);
				} else {

					p.getInventory().clear();

					p.setDisplayName(ChatColor.BLUE + p.getName() + ChatColor.WHITE);
					p.setPlayerListName(ChatColor.BLUE + p.getName() + ChatColor.WHITE);

					helmetMeta.setColor(Color.BLUE);
					chestplateMeta.setColor(Color.BLUE);
					legginsMeta.setColor(Color.BLUE);
					bootsMeta.setColor(Color.BLUE);
					magicKeyMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Magische "
							+ ChatColor.GOLD + "" + ChatColor.BOLD + "Sleutel");
					magicSwordMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Magische "
							+ ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Zwaard");
					magicSwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 10, true);

					helmet.setItemMeta(helmetMeta);
					chestplate.setItemMeta(chestplateMeta);
					leggins.setItemMeta(legginsMeta);
					boots.setItemMeta(bootsMeta);
					magicSword.setItemMeta(magicSwordMeta);
					magicKey.setItemMeta(magicKeyMeta);

					p.getInventory().setHelmet(helmet);
					p.getInventory().setChestplate(chestplate);
					p.getInventory().setLeggings(leggins);
					p.getInventory().setBoots(boots);
					p.getInventory().addItem(magicKey);
					p.getInventory().addItem(magicSword);

					World world = Bukkit.getServer().getWorld(getConfig().getString("spawnPolitie.world"));
					double x = getConfig().getDouble("spawnPolitie.x");
					double y = getConfig().getDouble("spawnPolitie.y");
					double z = getConfig().getDouble("spawnPolitie.z");
					p.teleport(new Location(world, x, y, z));
					p.sendMessage(ChatColor.GREEN + "U wordt geteleporteerd naar de spawn van de polities.");

					criminalList.remove(p.getName());
					policeList.add(p.getName());
				}
			} else if (loc.clone().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)
					&& (loc.clone().add(0, -2, 0).getBlock().getType().equals(Material.LAPIS_ORE))) {

				if (criminalList.contains(p.getName())) {

					push(event);

				}

			}
		}
	}
}
